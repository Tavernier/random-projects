# Farkle

This code captures Farkle scores and is (hopefully) written in a way to expand to other games.  The high-level flow of data here is Google Sheet > App Script > Cloud Function > BigQuery Table > BigQuery Views > Data Studio.

## Google Sheets App Script

- Purpose
    - Send game scores to a Google Cloud Function.
- Expected Named Ranges
    - `API_KEY`
        - Single cell.
        - Value is passed as `X-API-KEY` header in request to Cloud Function.
    - `GAME_ID`
        - Single cell.
        - Value should be somewhat unique.
        - The script will set this to the epoch.
    - `GAME_TYPE`
        - Single cell.
        - Type of game being plaed (e.g. Farkle).
    - `PLAYER_NAMES`
        - Range of cells.
        - Should be a single row of the player names.
    - `PLAYER_SCORES`
        - Range of cells.
        - Select all cells under the players for their scores per round.
    - `SAVE_STATUS`
        - Single cell.
        - The script will write whether saving the game scores was a success.
- Local Development
    - None for now but I need to learn more about Javascript development.

## Cloud Function

- Purpose
    - Receive score data and write it to a BigQuery table.
- Runtime & Environment Variables
    - Python 3.8
    - `API_KEY_HASH`: SHA256 hashdigest of the expected API Key passed by user.
- Signature
    - Headers
        - `X-API-KEY`: API Key to authorize.
    - Methods
        - `post`
            - Data should be in json format.
            - See [test_game.json](test_game.json) for example structure.
    - URL
        - The Cloud Function is deployed to: https://us-central1-jontav-production.cloudfunctions.net/scores
- Local Development
    - Start with `./up`.
    - Local Cloud Function endpoint will be available at http://localhost:8080/

### Examples

```shell
###############################################################################
# Local Function
###############################################################################
# returns a 405 since GET is unsupported
curl -IX GET http://localhost:8080

# record the test game
curl -X POST -H "X-API-KEY: ${API_KEY}" http://localhost:8080 \
    -d @test_game.json \
    --header "Content-Type: application/json"


###############################################################################
# Cloud Function
###############################################################################
# returns a 405 since GET is unsupported
curl -IX GET https://us-central1-jontav-production.cloudfunctions.net/scores

# record the test game
curl -X POST -H "X-API-KEY: ${API_KEY}" https://us-central1-jontav-production.cloudfunctions.net/scores \
    -d @test_game.json \
    --header "Content-Type: application/json"
```

## BigQuery

- Purpose
    - The raw game data is inserted into the `games.scores` table.
    - Varous views exist to make this raw data easier to use.
        - `players`: One row for each player with lifetime stats.
        - `games`: One row for each game.
        - `player_games`: One row for each game and player.
        - `rounds`: One row for each game, round, and player.
    - Unfortunately, I see no way to add column descriptions to views via Terraform.
- Local Development
    - None. No emulator exists.

## Data Studio

- I created a report but need to learn more about Data Studio to make something I like.
- Connecting to BigQuery and creating charts is easy.

## Deployment

- Run `./deploy` to launch the entire stack.
- Or, navigate to `./iac` and run `terraform` commands as needed.
