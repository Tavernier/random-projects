// Return true when score is a valid Farkle score.
function IS_VALID_FARKLE_SCORE(input) {
  return input % 50 == 0;
}


// Add menu bar so person can save game.
function onOpen(e) {
  console.info("Adding menu option.");
  let ui = SpreadsheetApp.getUi();
  ui.createMenu('Farkle Battle Royale')
      .addItem('Save Game', 'recordGame')
      .addToUi();
}


// Returns seconds since UTC epoch.
function getEpoch() {
  const now = new Date();
  const utcMilllisecondsSinceEpoch = now.getTime() + (now.getTimezoneOffset() * 60 * 1000);
  const utcSecondsSinceEpoch = Math.round(utcMilllisecondsSinceEpoch / 1000);
  return utcSecondsSinceEpoch;
}


function getGamePayload(sheet){
  console.info("Getting game payload.");

  let playerNames = sheet.getRange("PLAYER_NAMES").getValues()[0];
  let playerScores = sheet.getRange("PLAYER_SCORES").getValues();
  
  let allScores = [];
  for (let playerIndex = 0; playerIndex < playerNames.length; playerIndex++) {
    playerName = playerNames[playerIndex];
    if (playerName){
      let scores = [];
      for (let row of playerScores) {
        if (row[playerIndex]){
          scores.push(row[playerIndex]);
        }else if (row[playerIndex] == "0"){
          scores.push(0);
        }
      }
      allScores.push({"player_name": playerName, "scores": scores});
    }
  }
  
  return {
    "game_id": sheet.getRange("GAME_ID").getValue(),
    "game_type": sheet.getRange("GAME_TYPE").getValue(),
    "created_at": new Date().toISOString(),
    "scores": allScores
  }
  
}

function postGameData(apiKey, gamePayload) {
  console.log("Posting game data to Cloud Function.");
  
  let http_options = {
    "method": "post",
    "contentType": "application/json",
    "headers": {
      "X-API-KEY": apiKey,
      "Content-Type": "application/json"
    },
    "payload": JSON.stringify(gamePayload)
  };
  
  return UrlFetchApp.fetch("https://us-central1-jontav-production.cloudfunctions.net/scores", http_options);
}


function recordGame(){
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  let sheet = spreadsheet.getActiveSheet();
  
  if (sheet.getRange("GAME_ID").getValue()) {
    SpreadsheetApp.getUi()
    .alert('Must clear Game ID before saving new game.');
    return;
  }
  sheet.getRange("GAME_ID").setValue(getEpoch());
  sheet.getRange("SAVE_STATUS").setValue("");
  
  // Make sheet data suitable for BigQuery.
  try {
    gamePayload = getGamePayload(sheet);
    // uncomment for debugging
    // console.log(JSON.stringify(gamePayload));
  }
  catch(error) {
    sheet.getRange("SAVE_STATUS").setValue("🥵 Unable to get game data from sheet.");
    return;
  }
  
  // Send game data to Cloud Function, which writes to BigQuery.
  try {
    let apiKey = sheet.getRange("API_KEY").getValue();
    let response = postGameData(apiKey, gamePayload);
    sheet.getRange("SAVE_STATUS").setValue("✅ Game saved to BigQuery.");
  }
  catch(error) {
    console.log(error);
    sheet.getRange("SAVE_STATUS").setValue("🥵 Failed to save game.");
  }
}
