#! /usr/bin/env python3

import hashlib
import json
import os

from google.cloud import bigquery


# sha256 of api key to authorize request.
# good enough security for the purpose of this cloud function.
API_KEY_HASH = os.environ["API_KEY_HASH"]


def is_authorized(request):
    """Returns true if user is authorized to perform request."""
    user_provided_api_key = request.headers.get("X-API-KEY", None)

    if user_provided_api_key is None:
        return False

    user_provided_api_key_hash = hashlib.sha256(user_provided_api_key.encode("utf=8"))
    if user_provided_api_key_hash.hexdigest() == API_KEY_HASH:
        return True

    return False


def entrypoint(request):
    """Receives the game data and inserts into BigQuery.
    
    Only accepts POST requests.
    Game data should already be structured to match destination BigQuery
    table fields, nullability, and data types.
    In other words, this function performs no data transformations.
    """
    if request.method.upper() != "POST":
        return "Method Not Allowed", 405

    user_provided_api_key = request.args.get("api_key", None)

    if not is_authorized(request):
        return "Unauthorized", 401

    game_data = request.get_json(force=True)

    client = bigquery.Client("jontav-production")
    errors = client.insert_rows_json("games.scores", [game_data])

    if errors:
        crap = {
            "errors": errors,
            "payload": game_data
        }
        return json.dumps(crap), 400
    else:
        return "Game saved.", 201

    return "uh oh", 500
