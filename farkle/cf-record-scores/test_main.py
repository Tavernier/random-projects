#! /usr/bin/env python3

import hashlib
import os
import unittest

# add these environment values for testing only.
# never store real creds in version control of course.
API_KEY = "i-am-the-secret!"
API_KEY_HASH = hashlib.sha256(API_KEY.encode("utf=8")).hexdigest()

os.environ.update({"API_KEY_HASH": API_KEY_HASH})

from main import is_authorized

class Request(object):
    """Used to fake a requests Request object."""
    def __init__(self, headers):
        self.headers = headers


class TestAuthorized(unittest.TestCase):
    """Verify authorization works as expected."""
    def test_no_key(self):
        self.assertFalse(is_authorized(Request({})))

    def test_invalid_key(self):
        request = Request({"X-API-KEY": "bogus-key!"})
        self.assertFalse(is_authorized(request))

    def test_valid_key(self):
        request = Request({"X-API-KEY": "i-am-the-secret!"})
        self.assertTrue(is_authorized(request))


if __name__ == "__main__":
    unittest.main()
