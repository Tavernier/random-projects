###############################################################################
# BigQuery Dataset
###############################################################################
resource "google_bigquery_dataset" "games" {
  dataset_id                  = "games"
  description                 = "Game scores and whatnot."
  location                    = "US"
}


###############################################################################
# Landing Table
# Scores will be written here by the Cloud Function
###############################################################################
resource "google_bigquery_table" "scores" {
  dataset_id = google_bigquery_dataset.games.dataset_id
  table_id   = "scores"

  schema = <<EOF
[
  {
    "name": "game_id",
    "description": "Game ID.",
    "type": "STRING"
  },
  {
    "name": "game_type",
    "description": "Type of game such as Farkle.",
    "type": "STRING"
  },
  {
    "name": "created_at",
    "description": "When this data was created.",
    "type": "TIMESTAMP"
  },
  {
    "name": "scores",
    "description": "Rows of spreadsheet data.",
    "type": "RECORD",
    "mode": "REPEATED",
    "fields": [
        {
          "name": "player_name",
          "description": "Player name.",
          "type": "STRING"
        },
        {
          "name": "scores",
          "description": "Scores for the player's rounds.",
          "type": "INTEGER",
          "mode": "REPEATED"
        }
    ]
  }
]
EOF
}

###############################################################################
# Rounds
# Take the raw data and transform it into game rounds.
# So, one row per game + round + player.
###############################################################################
resource "google_bigquery_table" "rounds" {
  dataset_id = google_bigquery_dataset.games.dataset_id
  table_id   = "rounds"
  description = "Game rounds per player."

  depends_on = [
    google_bigquery_table.scores
  ]

  view {
    use_legacy_sql = false
    query = <<EOF
      select
        -- finally, add an indicator showing whether
        -- the player won the round.
        -- key
        b.game_id,
        b.round_id,
        b.player_name,
        --values
        b.score,
        b.running_total,
        b.round_rank,
        case
          when b.round_rank = 1 then 1
          else 0
        end as is_round_winner
      from (
        select
          -- then add the player's ranking per round
          -- based on the running total score.
          *,
          rank() over(
            partition by game_id, round_id
            order by running_total desc
          ) as round_rank
        from (
          -- first, we'll add a running total to each round.
          select
            -- key
            g.game_id,
            s.player_name,
            offset + 1 as round_id,

            -- values
            score,
            sum(score) over(
              partition by g.game_id, s.player_name
              order by offset
            ) as running_total
          from games.scores g
          join g.scores s
          join unnest(s.scores) as score with offset
        ) a
      ) b
      order by game_id, round_id, round_rank, player_name
      EOF
  }
}

###############################################################################
# Player Games
# Aggregate round information into player+game-level information.
# So, one row per game + player.
###############################################################################
resource "google_bigquery_table" "player_games" {
  dataset_id = google_bigquery_dataset.games.dataset_id
  table_id   = "player_games"
  description = "Player information for each game."

  depends_on = [
    google_bigquery_table.rounds
  ]

  view {
    use_legacy_sql = false
    query = <<EOF
    select
      -- key
      a.game_id,
      a.player_name,
      -- values
      a.score,
      a.game_rank,
      case
        when a.game_rank = 1 then 1
        else 0
      end as is_game_winner
    from (
      select
        r.game_id,
        r.player_name,
        sum(r.score) as score,
        rank() over(
          partition by r.game_id
          order by sum(r.score) desc
        ) as game_rank
      from games.rounds r
      group by
        r.game_id,
        r.player_name
    ) a
    order by game_id, game_rank
    EOF
  }
}

###############################################################################
# Games
# Aggregate lower level data into game-level data.
# So, one row per game.
###############################################################################
resource "google_bigquery_table" "games" {
  dataset_id = google_bigquery_dataset.games.dataset_id
  table_id   = "games"
  description = "Game-lvel information."

  depends_on = [
    google_bigquery_table.scores,
    google_bigquery_table.rounds,
    google_bigquery_table.player_games
  ]

  view {
    use_legacy_sql = false
    query = <<EOF
      with game_stats as (
        select 
          g.game_id,
          max(r.round_id) as rounds_played,
          sum(r.score) as total_score,
          count(distinct r.player_name) as total_players,
        from games.scores g
        join games.rounds r
          on g.game_id = r.game_id
        group by
          g.game_id
      )

      select
        g.game_id,
        g.game_type,
        g.created_at,
        pg.player_name as winning_player,
        pg.score as winning_score,
        gs.rounds_played,
        gs.total_score,
        gs.total_players
      from games.scores g
      join games.player_games pg
        on g.game_id = pg.game_id
        and pg.is_game_winner = 1
      join game_stats gs
        on g.game_id = gs.game_id
    EOF
  }
}

###############################################################################
# Players
# Finally, aggregate lifetime player stats.
# Stats per custom timeframes will be handled at the visualization layer.
# So, one row per player here.
###############################################################################
resource "google_bigquery_table" "players" {
  dataset_id = google_bigquery_dataset.games.dataset_id
  table_id   = "players"
  description = "Player-lvel information."

  depends_on = [
    google_bigquery_table.scores,
    google_bigquery_table.rounds
  ]

  view {
    use_legacy_sql = false
    query = <<EOF
      with players as (
        select distinct s.player_name
        from games.scores g
        join g.scores s
      ), game_stats as (
        select
          -- key
          pg.player_name,
          -- values
          sum(pg.score) as total_score,
          count(pg.game_id) as games_played,
          sum(pg.is_game_winner) as games_won,
          countif(pg.is_game_winner = 0) as games_lost,
          sum(pg.is_game_winner) / count(pg.game_id) as games_won_ratio
        from games.player_games pg
        group by
          pg.player_name
      ), round_stats as (
        select
          -- key
          r.player_name,
          -- values
          count(r.round_id) as rounds_played,
          sum(r.is_round_winner) as rounds_won,
          countif(r.is_round_winner = 0) as rounds_lost,
          sum(r.is_round_winner) / count(r.round_id) as rounds_won_ratio
        from games.rounds r
        group by
          r.player_name
      )

      select
        -- key
        p.player_name,
        -- values (lifetime stats)
        gs.total_score,
        gs.games_played,
        gs.games_won,
        gs.games_lost,
        gs.games_won_ratio,
        rs.rounds_played,
        rs.rounds_won,
        rs.rounds_lost,
        rs.rounds_won_ratio
      from players p
      join game_stats gs
        on p.player_name = gs.player_name
      join round_stats rs
        on p.player_name = rs.player_name
    EOF
  }
}
