# This uploads the source file to the bucket.
# So no need to do that with gsutil or anything.
resource "google_storage_bucket_object" "scores" {
  bucket = "jontav-production"
  name   = "functions/cf-record-scores-${var.cf_version}.zip"
  source = "/tmp/cf-record-scores.zip"
}

resource "google_cloudfunctions_function" "scores" {
  name        = "scores"
  description = "Record scores to BigQuery"
  runtime     = "python38"

  source_archive_bucket = "jontav-production"
  source_archive_object = google_storage_bucket_object.scores.name

  trigger_http          = true
  entry_point           = "entrypoint"
  ingress_settings      = "ALLOW_ALL"
  max_instances         = 3

  service_account_email = google_service_account.farkle.email

  environment_variables = {
    API_KEY_HASH = var.api_key_hash
  }
}


