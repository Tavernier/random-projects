###############################################################################
# Service Account
# The Cloud Function (CF) will run as this service account.
###############################################################################
resource "google_service_account" "farkle" {
  account_id   = "cf-record-scores"
  display_name = "Record Scores"
}

###############################################################################
# BigQuery Permissions
# Since the CF writes to BigQuery, let's add the access here.
###############################################################################

resource "google_bigquery_dataset_iam_member" "editor" {
  project    = google_bigquery_table.scores.project
  dataset_id = google_bigquery_table.scores.dataset_id
  role       = "roles/bigquery.metadataViewer"
  member     = "serviceAccount:${google_service_account.farkle.email}"
}

resource "google_bigquery_table_iam_member" "scores" {
  project = google_bigquery_table.scores.project
  dataset_id = google_bigquery_table.scores.dataset_id
  table_id = google_bigquery_table.scores.table_id
  role = "roles/bigquery.dataOwner"
  member = "serviceAccount:${google_service_account.farkle.email}"
}

###############################################################################
# Cloud Function Permissions
# Permit anyone in the world to access the function.
# Authorization is controlled with an API Key.
###############################################################################
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.scores.project
  region         = google_cloudfunctions_function.scores.region
  cloud_function = google_cloudfunctions_function.scores.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
