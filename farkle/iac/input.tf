variable "api_key_hash" {
  type        = string
  description = "SHA256 hexdigest of API Key."
}

variable "cf_version" {
  type        = string
  description = "Unique version for Cloud Function zip archive."
}
