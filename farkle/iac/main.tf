terraform {
  backend "gcs" {
    bucket  = "jontav-production"
    prefix  = "terraform/farkle-state"
  }
}

provider "google" {
  project     = "jontav-production"
  region      = "us-central1"
}
