# Starters

This is helpful starter code that comes up for me from time to time.

## Running the Application

```shell
# start the application and store data in to MongoDB.
# see documentation below for available data stores.
export DATA_STORE=mongodb
./up.sh

# interact with the FastAPI app or run tests.
# to run tests, go to the app's directory, install requirements, then run pytest:
pushd app
pip install -r requirements.txt -r test_requirements.txt
pytest -v --cov --cov-report=term-missing
popd

./down.sh


```

## Available Data Stores

You can set these environment variables and make sure they line up with what's in the Docker Compsoe file.

- `DATA_STORE`: determines data store that will be used. The default is `memory`.  Other options are:
    - `export DATA_STORE=redis` will run Redis on port 6379.
    - `export DATA_STORE=firestore` will run GCP Firestore Emulator on port 9000.
    - `export DATA_STORE=elasticsearch` will start Elasticsearch on port 9200 and Kibana on port 9201.
    - `export DATA_STORE=mongodb` will start MongoDB on port 27017 and Mongo Express UI on port 27018.
- `DATA_STORE_HOST`: Data store's host. Leave blank to use the Docker Compose default.
- `DATA_STORE_PORT`: Data store's port. Leave blank to use the Docker Compose default.
- `DATA_STORE_DB_NAME`: Data store's database name, if applicable. Leave blank to use the Docker Compose default.
- `DATA_STORE_USERNAME`: Data store's login username, if applicable. Leave blank to use the Docker Compose default.
- `DATA_STORE_PASSWORD`: Data store's login password, if applicable. Leave blank to use the Docker Compose default.
- `DATA_STORE_API_KEY`: Data store's API key, if applicable.

### To Add

- Upgrade MongoDB version
- Xata
