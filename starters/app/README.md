# Start FastAPI App

This is an example FastAPI where I tried to apply concepts and patterns from part 1 of the [Architecture Patterns with Python](https://www.cosmicpython.com/book/preface.html) book.

## Tests

```shell
pip install -r requirements.txt -r test_requirements.txt
pytest -v --cov --cov-report term-missing
```
