#! /usr/bin/env python3
"""Product Manager HTTP Service

This is just an interface into the Product Manager service layer.
The code in this file should be mainly concerned with HTTP stuff.
Business logic exists in the Domain Models.
Interacting with those models exists in the Service Layer.
"""

from datetime import datetime
from typing import Optional

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, Field

import repos
import service

app = FastAPI(title="Product Manager")
repo = repos.repo_factory()

class HelloWorld(BaseModel):
    message: str
    effective_at: datetime = Field(default_factory=datetime.utcnow)

@app.get("/", response_model=HelloWorld)
def get_root():
    return HelloWorld(message="Hello world!")


###############################################################################
# Get a Product
###############################################################################
class ProductOut(BaseModel):
    sku: str
    title: str
    price: float
    version: int
    is_discontinued: bool
    created_at: datetime
    updated_at: datetime
    discontinued_at: Optional[datetime]
    effective_at: datetime = Field(default_factory=datetime.utcnow)

@app.get("/api/products/{sku}", response_model=ProductOut)
def get_product(sku: str):
    product = service.get_product(sku, repo)
    if not product:
        raise HTTPException(status_code=404, detail=f"Product not found with sku {sku}.")
    return ProductOut(
        sku=product.sku,
        title=product.title,
        price=product.price,
        version=product.version,
        is_discontinued=product.is_discontinued,
        created_at=product.created_at,
        updated_at=product.updated_at,
        discontinued_at=product.updated_at,
    )

###############################################################################
# Create a Product
###############################################################################
class NewProduct(BaseModel):
    title: str
    price: float

@app.put("/api/products/{sku}", response_model=ProductOut)
def create_product(sku: str, new_product: NewProduct):
    try:
        product = service.create_product(sku, new_product.title, new_product.price, repo)
        return product
    except service.ConflictException as e:
        raise HTTPException(status_code=409, detail=str(e))

###############################################################################
# Update a Product
###############################################################################
class PatchProduct(BaseModel):
    price: float

###############################################################################
# Delete a Product
###############################################################################
class DeletedProductOut(BaseModel):
    sku: str
    deleted_at: datetime
    effective_at: datetime = Field(default_factory=datetime.utcnow)

@app.delete("/api/products/{sku}", response_model=DeletedProductOut)
def create_product(sku: str):
    deleted = service.delete_product(sku, repo)
    if deleted:
        return DeletedProductOut(sku=deleted.sku, deleted_at=deleted.deleted_at)
    else:
        raise HTTPException(status_code=404, detail=f"Product not found with sku {sku}.")
