#! /usr/bin/env python3
"""Domain Models

These models include business logic.
Other parts of the app can use these.
Avoid adding dependencies here.
"""

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field

class Product(BaseModel):
    sku: str
    title: str
    price: float
    version: Optional[int] = Field(0, ge=0)
    is_discontinued: Optional[bool] = Field(False)
    created_at: datetime = Field(default_factory=datetime.utcnow)
    updated_at: datetime = Field(default_factory=datetime.utcnow)
    discontinued_at: Optional[datetime] = Field(None)

    def _update(self, when: datetime = None):
        self.updated_at = when or datetime.utcnow()
        self.version = self.version + 1

    def update_price(self, new_price: float):
        self.price = new_price
        self._update()

    def discontinue(self):
        if not self.is_discontinued:
            self.is_discontinued = True
            now = datetime.utcnow()
            self.discontinued_at = now
            self._update(now)

class DeletedProduct(BaseModel):
    sku: str
    deleted_at: datetime
