
import os

from repos.abstract import AbstractRepo
from repos.elasticsearch_repo import ElasticsearchRepo
from repos.firestore import FirestoreRepo
from repos.memory import DictionaryRepo
from repos.mongodb import MongoRepo
from repos.redis_repo import RedisRepo
from repos.xata_repo import XataRepo

__all__ = [
    AbstractRepo,
    DictionaryRepo,
    ElasticsearchRepo,
    FirestoreRepo,
    MongoRepo,
    RedisRepo,
    XataRepo
]

def repo_factory(products_collection_name="products", deleted_products_collection_name="deleted-products") -> AbstractRepo:
    match os.getenv("DATA_STORE", "memory").lower():
        case "elasticsearch":
            repo = ElasticsearchRepo(products_collection_name, deleted_products_collection_name)
        case "firestore":
            if "FIRESTORE_EMULATOR_HOST" not in os.environ:
                os.environ["FIRESTORE_EMULATOR_HOST"] = "localhost:9000"
            repo = FirestoreRepo(products_collection_name, deleted_products_collection_name)
        case "mongodb":
            repo = MongoRepo(products_collection_name, deleted_products_collection_name)
        case "redis":
            repo = RedisRepo(products_collection_name, deleted_products_collection_name)
        case "xata":
            repo = XataRepo(products_collection_name, deleted_products_collection_name)
        case _:
            repo = DictionaryRepo(products_collection_name, deleted_products_collection_name)
    return repo
