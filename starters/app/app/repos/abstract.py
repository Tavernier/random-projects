#! /usr/bin/env python3
"""Abstraction over actual Data Stores"""

from abc import ABC, abstractmethod

class AbstractRepo(ABC):  # pragma: no cover
    @abstractmethod
    def get(self, entity_id: str):
        pass

    @abstractmethod
    def upsert(self, entity_id: str, entity: dict):
        pass

    @abstractmethod
    def delete(self, entity_id: str):
        pass
