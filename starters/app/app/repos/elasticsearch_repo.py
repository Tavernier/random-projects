#! /usr/bin/env python3
"""Elasticsearch Repository"""

import os
from datetime import datetime

from elasticsearch import Elasticsearch, NotFoundError

from repos import AbstractRepo


class ElasticsearchRepo(AbstractRepo):
    def __init__(self, collection: str, delete_collection: str):
        self.index_name = collection
        self.delete_index_name = delete_collection
        self._client = None
        self._index_exists = False
        self._delete_index_exists = False

    @property
    def client(self):
        if self._client is None:
            self._client = Elasticsearch(os.getenv("DATA_STORE_HOST", "http://localhost:9200"))
        return self._client

    @property
    def index_exists(self):
        if self.client.indices.exists(index=self.index_name):
            self._index_exists = True
        else:
            self.client.indices.create(
                index=self.index_name,
                mappings={
                            "properties": {
                                "sku": {"type": "keyword"},
                                "title": {"type": "text"},
                                "price": {"type": "double"},
                                "created_at": {"type": "date"},
                                "is_high_priced": {"type": "boolean"},
                            }
                        },
                settings={"number_of_replicas": 0, "number_of_shards": 1},
                )
            self._index_exists = True
        return self._index_exists
    
    @property
    def delete_index_exists(self):
        if self.client.indices.exists(index=self.delete_index_name):
            self._delete_index_exists = True
        else:
            self.client.indices.create(
                index=self.delete_index_name,
                mappings={"properties": {"sku": {"type": "keyword"}, "deleted_at": {"type": "date"}}},
                settings={"number_of_replicas": 0, "number_of_shards": 1},
                )
            self._delete_index_exists = True
        return self._delete_index_exists

    def get(self, entity_id: str, deleted: bool = False) -> dict:
        if not self.index_exists or (deleted and not self.delete_index_exists):
            return None
        try:
            index = self.delete_index_name if deleted else self.index_name
            doc = self.client.get(index=index, id=entity_id)
            return doc["_source"]
        except NotFoundError:
            return None

    def upsert(self, entity_id: str, entity: dict) -> dict:
        if not self.index_exists:
            return None
        self.client.index(index=self.index_name, id=entity_id, document=entity)
        return self.get(entity_id)

    def delete(self, entity_id: str) -> dict:
        if not self.index_exists or not self.delete_index_exists:
            return None
        try:
            deleted = self.client.delete(index=self.index_name, id=entity_id)
            self.client.index(index=self.delete_index_name, id=entity_id, document={"id": deleted["_id"], "deleted_at": datetime.utcnow()})
            return self.get(entity_id, True)
        except NotFoundError:
            return None
