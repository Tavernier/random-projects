#! /usr/bin/env python3
"""Firestore Repository"""

import os
from datetime import datetime

from google.cloud.firestore import Client

from repos import AbstractRepo

class FirestoreRepo(AbstractRepo):
    """Read, Write, and Delete data stored in GCP Firestore."""
    def __init__(self, collection, delete_collection):
        self.collection = collection
        self.delete_collection = delete_collection
        self._client = None

    @property
    def client(self):
        if not self._client:
            self._client = Client(project=os.getenv("GCP_PROJECT", "local-project"))
        return self._client

    def get(self, entity_id: str, deleted: bool = False) -> dict:
        """Returns the a dictionary of the retrieved Firestore document."""
        if deleted:
            collection_name = self.delete_collection
        else:
            collection_name = self.collection
        reference = self.client.collection(collection_name).document(entity_id)
        document = reference.get()
        if document.exists:
            return dict(document.to_dict())
        else:
            return None

    def upsert(self, entity_id: str, entity: dict) -> dict:
        """Write a document to Firestore."""
        self.client.collection(self.collection).document(entity_id).set(entity)
        return self.get(entity_id)

    def delete(self, entity_id: str):
        reference = self.client.collection(self.collection).document(entity_id)
        if reference.get().exists:
            reference.delete()
            self.client.collection(self.delete_collection).document(entity_id).set(
                {"id": entity_id, "deleted_at": datetime.utcnow()}
            )
        return self.get(entity_id, True)
