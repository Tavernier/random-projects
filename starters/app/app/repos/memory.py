#! /usr/bin/env python3
"""In-memory Data Store"""

from datetime import datetime

from repos import AbstractRepo

class DictionaryRepo(AbstractRepo):
    """Read, Write, and Delete data stored in an in-memory Dictionary."""
    def __init__(self, collection: str, delete_collection: str):
        self.collection = collection
        self.delete_collection = delete_collection
        self._store = dict()
        self._store[collection] = dict()
        self._store[delete_collection] = dict()

    def get(self, entity_id: str, deleted: bool = False) -> dict:
        if deleted:
            return self._store[self.delete_collection].get(entity_id, None)
        else:
            return self._store[self.collection].get(entity_id, None)

    def upsert(self, entity_id: str, entity: dict) -> dict:
        self._store[self.collection][entity_id] = entity
        return self.get(entity_id)

    def delete(self, entity_id: str) -> dict:
        try:
            self._store[self.collection].pop(entity_id)
            self._store[self.delete_collection][entity_id] =  {"id": entity_id, "deleted_at": datetime.utcnow()}
        except KeyError:
            return None
        return self.get(entity_id, True)

