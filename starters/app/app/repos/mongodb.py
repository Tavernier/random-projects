#! /usr/bin/env python3
"""MongoDB Repository"""

import os
from datetime import datetime

from pymongo import MongoClient

from repos import AbstractRepo

class MongoRepo(AbstractRepo):
    """Read, Write, and Delete data stored in MongoDB."""
    def __init__(self, collection: str, delete_collection: str):
        self.collection = collection
        self.delete_collection = delete_collection
        self.host = os.getenv("DATA_STORE_HOST", "localhost")
        self.port = int(os.getenv("DATA_STORE_PORT", "27017"))
        self.db = os.getenv("DATA_STORE_DB_NAME", "repo-tests")
        self._client = None

    @property
    def client(self):
        if self._client is None:
            self._client = MongoClient(
                host=self.host,
                port=self.port,
                username=os.getenv("DATA_STORE_USERNAME", "tech-is-fun"),
                password=os.getenv("DATA_STORE_PASSWORD", "tech-is-fun"),
            )
        return self._client

    def get(self, entity_id: str, deleted: bool = False) -> dict:
        db = self.client[self.db]
        if deleted:
            collection = db[self.delete_collection]
        else:
            collection = db[self.collection]
        document = collection.find_one({"_id": entity_id})
        return document

    def upsert(self, entity_id: str, entity: dict) -> dict:
        db = self.client[self.db]
        collection = db[self.collection]
        document = entity.copy()
        collection.replace_one({"_id": entity_id}, document, upsert=True)
        return self.get(entity_id)

    def delete(self, entity_id: str) -> dict:
        db = self.client[self.db]
        collection = db[self.collection]
        delete_collection = db[self.delete_collection]
        delete_result = collection.delete_one({"_id": entity_id})
        if delete_result.deleted_count == 1:
            delete_collection.replace_one({"_id": entity_id}, {"id": entity_id, "deleted_at": datetime.utcnow()}, upsert=True)
            return self.get(entity_id, True)
        else:
            return None
