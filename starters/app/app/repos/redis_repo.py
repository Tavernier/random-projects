#! /usr/bin/env python3
"""Redis Repository

This file is named reds_repo to avoid conflicts with the redis library.
"""

import json
import os
from datetime import datetime

import redis

from repos import AbstractRepo

class RedisRepo(AbstractRepo):
    def __init__(self, collection: str, delete_collection: str):
        self.collection = collection
        self.delete_collection = delete_collection
        self.host = os.getenv("DATA_STORE_HOST", "localhost")
        self.port = int(os.getenv("DATA_STORE_PORT", "6379"))
        self._client = None

    def key(self, entity_id: str):
        return ":".join([self.collection, entity_id])

    def deleted_key(self, entity_id: str):
        return ":".join([self.delete_collection, entity_id])

    @property
    def client(self):
        if self._client is None:    
            self._client = redis.Redis(host=self.host, port=self.port, decode_responses=True)
        return self._client

    def get(self, entity_id: str, deleted: bool = False) -> dict:
        if deleted:
            redis_key = self.deleted_key(entity_id)
        else:
            redis_key = self.key(entity_id)
        document = self.client.get(redis_key)
        if document:
            return json.loads(document)
        else:
            return None

    def upsert(self, entity_id: str, entity: dict) -> dict:
        redis_key = self.key(entity_id)
        self.client.set(redis_key, json.dumps(entity, default=str))
        return self.get(entity_id)

    def delete(self, entity_id: str) -> dict:
        redis_key = self.key(entity_id)
        if self.client.exists(redis_key):
            self.client.delete(redis_key)
            deleted = {"id": entity_id, "deleted_at": datetime.utcnow()}
            self.client.set(self.deleted_key(entity_id), json.dumps(deleted, default=str))
        return self.get(entity_id, True)