#! /usr/bin/env python3
"""Xata Repository

This file is named reds_repo to avoid conflicts with the xata library.
"""

import json
import logging
import os
from datetime import datetime

import requests.exceptions
import xata

from repos import AbstractRepo


def _json_dumps_format(value):
    if isinstance(value, datetime):
        if value.isoformat().endswith("+00:00"):
            return value.isoformat()
        else:
            return value.isoformat() + "Z"
    else:
        return str(value)

class XataRepo(AbstractRepo):
    def __init__(self, collection: str, delete_collection: str):
        self.collection = collection
        self.delete_collection = delete_collection
        self.host = os.getenv("DATA_STORE_HOST", "https://jon-tavernier-s-workspace-tmtbne.us-east-1.xata.sh/db/product-manager")
        self.api_key = os.environ["DATA_STORE_API_KEY"]
        self._client = None
        self.logger = logging.getLogger(name=self.__class__.__name__)

    @property
    def client(self):
        if self._client is None:
            self._client = xata.XataClient(db_url=self.host, api_key=self.api_key)
        return self._client

    def get(self, entity_id: str, deleted: bool = False) -> dict:
        if deleted:
            collection = self.delete_collection
        else:
            collection = self.collection
        api_response = self.client.records().get(collection, entity_id)
        if api_response.is_success():
            return api_response.response.json()
        else:
            return None

    def _xata_payload(self, data: dict):
        # xata's client passes the dict directly to requests and i don't have the ability
        # to tell it to serialize datatime values as strings from what i can see.
        return json.loads(json.dumps(data, default=_json_dumps_format))

    def upsert(self, entity_id: str, entity: dict) -> dict:
        api_response = self.client.records().insert_with_id(self.collection, entity_id, self._xata_payload(entity))
        try:
            api_response.response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            self.logger.error(json.dumps(e.response.json()))
            self.logger.debug(entity)
            self.logger.debug(self._xata_payload(entity))
            raise
        return self.get(entity_id)

    def delete(self, entity_id: str) -> dict:
        # xata's documentation states that if delete is called on a non-existent ID, it will return a
        # 404. however, it actually always seems to return a 204. so, i need to work around that.
        get_response = self.client.records().get(self.collection, entity_id)
        if get_response.status_code == 404:
            return None

        api_response = self.client.records().delete(self.collection, entity_id)
        try:
            api_response.response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            self.logger.error(json.dumps(e.response.json()))
            raise

        api_response = self.client.records().insert_with_id(self.delete_collection, entity_id, self._xata_payload({"deleted_at": datetime.utcnow()}))
        try:
            api_response.response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            self.logger.error(json.dumps(e.response.json()))
            self.logger.debug(entity_id)
            raise

        return self.get(entity_id, True)
