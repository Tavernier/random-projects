#! /usr/bin/env python3
"""Service Layer

All clients must interact with products via this service layer.
In this context, clients mean an HTTP interface, CLI, etc.
"""

from datetime import datetime

import models
import repos

class ConflictException(Exception):
    pass

def get_product(sku: str, repo: repos.AbstractRepo) -> models.Product:
    entity = repo.get(sku)
    if not entity:
        return None
    product = models.Product.model_validate(entity)
    return product

def create_product(sku: str, title: str, price: float, repo: repos.AbstractRepo) -> models.Product:
    if repo.get(sku):
        raise ConflictException(f"Product with sku {sku} already exists.")
    now = datetime.utcnow()
    product = models.Product(sku=sku, title=title, price=price, version=1, created_at=now, updated_at=now)
    repo.upsert(product.sku, product.model_dump())
    return get_product(product.sku, repo)

def update_price(sku: str, new_price: float, repo: repos.AbstractRepo) -> models.Product:
    product = get_product(sku, repo)
    product.update_price(new_price)
    repo.upsert(product.sku, product.model_dump())
    return get_product(sku, repo)

def discontinue_product(sku: str, repo: repos.AbstractRepo) -> models.Product:
    product = get_product(sku, repo)
    product.discontinue()
    repo.upsert(product.sku, product.model_dump())
    return get_product(sku, repo)

def delete_product(sku: str, repo: repos.AbstractRepo) -> models.DeletedProduct:
    deleted = repo.delete(sku)
    if deleted:
        return models.DeletedProduct(sku=deleted["id"], deleted_at=deleted["deleted_at"])
    else:
        return None
