#! /usr/bin/env python3

from uuid import uuid4

import httpx
import pytest
from fastapi.testclient import TestClient

import main


@pytest.fixture(scope="module")
def client():
    return TestClient(main.app)

@pytest.fixture()
def sku():
    uuid = str(uuid4())
    return f"TPF-{uuid}"

def test_hello_world(client):
    response = client.get("/")
    response_data = response.json()
    assert response.status_code == 200
    assert response_data["message"] == "Hello world!"
    assert "effective_at" in response_data

def test_create_product(client, sku):
    response = client.put(f"/api/products/{sku}", json={"title": "Clear Glass", "price": 5.49})
    response_data = response.json()
    assert response.status_code == 200
    assert response_data["sku"] == sku
    assert response_data["title"] == "Clear Glass"
    assert response_data["price"] == 5.49
    assert response_data["version"] == 1
    assert response_data["is_discontinued"] == False
    assert response_data["created_at"] == response_data["updated_at"]
    assert "effective_at" in response_data
    assert response_data["discontinued_at"] is None

def test_get_product(client, sku):
    client.put(f"/api/products/{sku}", json={"title": "Clear Glass", "price": 5.49})
    response = client.get(f"/api/products/{sku}")
    response_data = response.json()
    assert response.status_code == 200
    assert response_data["sku"] == sku

def test_conflict(client, sku):
    client.put(f"/api/products/{sku}", json={"title": "Clear Glass", "price": 5.49})
    response = client.put(f"/api/products/{sku}", json={"title": "Clear Glass", "price": 5.49})
    assert response.status_code == 409
    with pytest.raises(httpx.HTTPStatusError) as e:
        response.raise_for_status()

def test_delete(client, sku):
    # first, create a product.
    response = client.put(f"/api/products/{sku}", json={"title": "Clear Glass", "price": 5.49})
    response_data = response.json()
    assert response.status_code == 200
    assert response_data["sku"] == sku
    # then, delete it.
    response = client.delete(f"/api/products/{sku}")
    response_data = response.json()
    assert response.status_code == 200
    assert response_data["sku"] == sku
    assert response_data["deleted_at"]
    assert "effective_at" in response_data
    # it should now be not found in both GET and DELETE
    response = client.get(f"/api/products/{sku}")
    assert response.status_code == 404
    response = client.delete(f"/api/products/{sku}")
    assert response.status_code == 404

def test_not_found(client):
    response = client.get("/api/products/TP-NOT-FOUND_BLAH")
    assert response.status_code == 404
    with pytest.raises(httpx.HTTPStatusError):
        response.raise_for_status()
