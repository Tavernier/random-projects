#! /usr/bin/env python3

from uuid import uuid4

import pytest

import models
import repos
import service

@pytest.fixture()
def repo():
    return repos.repo_factory()

@pytest.fixture()
def sku():
    uuid = str(uuid4())
    return f"TP-{uuid}"

def test_create_product(repo, sku):
    product = service.create_product(sku=sku, title="Cyan Shirt", price=17.32, repo=repo)
    assert isinstance(product, models.Product)
    assert product.sku == sku
    assert product.title == "Cyan Shirt"
    assert product.price == 17.32
    assert product.created_at == product.updated_at

def test_get_product(repo, sku):
    service.create_product(sku=sku, title="Gray Shirt", price=7.32, repo=repo)
    product = service.get_product(sku, repo)
    assert product.sku == sku
    assert product.title == "Gray Shirt"
    assert product.price == 7.32
    assert product.created_at == product.updated_at

def test_not_found(repo):
    product = service.get_product("BLAHBLAHBLAH", repo)
    assert product is None

def test_conflict(repo, sku):
    service.create_product(sku=sku, title="Green Shirt", price=7.32, repo=repo)
    with pytest.raises(service.ConflictException):
        service.create_product(sku=sku, title="Blue Shirt", price=3.32, repo=repo)

def test_delete(repo, sku):
    service.create_product(sku=sku, title="Green Shirt", price=7.32, repo=repo)
    assert service.get_product(sku, repo) is not None
    service.delete_product(sku, repo)
    assert service.get_product(sku, repo) is None

def test_discontinue(repo, sku):
    service.create_product(sku=sku, title="Gray Shirt", price=7.32, repo=repo)
    product = service.discontinue_product(sku, repo)
    assert product.sku == sku
    assert product.title == "Gray Shirt"
    assert product.price == 7.32
    assert product.is_discontinued and product.discontinued_at and product.discontinued_at == product.updated_at

def test_update(repo, sku):
    created_product = service.create_product(sku=sku, title="Green Shirt", price=7.32, repo=repo)
    updated_product = service.update_price(sku=sku, new_price=33.33, repo=repo)

    assert updated_product.version == 2
    assert updated_product.price == 33.33
    assert updated_product.updated_at > created_product.updated_at
