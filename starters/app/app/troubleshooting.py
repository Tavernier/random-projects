#! /usr/bin/env python3

import os
import time
from uuid import uuid4

import service
import repos

import requests.exceptions
import xata

if __name__ == "__main__":
    host = os.getenv("DATA_STORE_HOST", "https://jon-tavernier-s-workspace-tmtbne.us-east-1.xata.sh/db/product-manager")
    api_key = os.environ["DATA_STORE_API_KEY"]
    client = xata.XataClient(db_url=host, api_key=api_key)
    repo = repos.XataRepo("products", "deleted-products")
    
    sku = f"TXP-{uuid4()}"

    try:
        print(sku)
        print(service.create_product(sku, "Test Product", 3.33, repo))
        time.sleep(1)
        print(service.delete_product(sku, repo))
        time.sleep(1)
        print(service.delete_product(sku, repo))
        time.sleep(1)
        print(service.delete_product("BLAHBLAHB", repo))
    except requests.exceptions.HTTPError as e:
        print(e)
        print(dir(e))
        print(e.response.json())
        raise
