#! /usr/bin/env bash

set -euo pipefail

nuke_the_stack(){
    docker compose down --remove-orphans --volumes
}

main(){
    nuke_the_stack
}

main
