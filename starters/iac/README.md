# Infrastructure

This includes a starter set for GCP infrastructure using Terragrunt and Terraform.

For each deployable thing:

```shell
terragrunt init
terragrunt apply
```
