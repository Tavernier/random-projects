###############################################################################
# Terraform and Terragrunt Setup
###############################################################################
provider "google" {
  project = var.project
}

terraform {
  backend "gcs" {}
}

# we sometimes need the google project number and this block lets us do that.
# use ${data.google_project.project.number} where needed
data "google_project" "project" {
}

###############################################################################
# Tips and Tricks
###############################################################################
# Load a json file: file("${path.module}/schemas/your_file.json")
