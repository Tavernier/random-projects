resource "google_pubsub_schema" "this" {
  name = var.topic_name
  type = "AVRO"
  definition = var.avro_schema_definition
}

resource "google_pubsub_topic" "this" {
  name   = var.topic_name
  labels = {}
  schema_settings {
    schema   = google_pubsub_schema.this.id
    encoding = "JSON"
  }
}

resource "google_pubsub_subscription" "this" {
  name  = "${var.topic_name}-to-bq"
  topic = google_pubsub_topic.this.name
  retain_acked_messages = true

  bigquery_config {
    table            = "${var.bq_project}:${var.bq_dataset}.${var.bq_table}"
    use_topic_schema = true
  }
}
