variable "topic_name" {
  type          = string
  description   = "PubSub Topic Name"
}

variable "avro_schema_definition" {
  type          = string
  description   = "Avro Schema definition."
}

variable "bq_project" {
  type          = string
  description   = "BigQuery Prjoect ID of the target table."
}

variable "bq_dataset" {
  type          = string
  description   = "BigQuery Dataset ID of the target table."
}

variable "bq_table" {
  type          = string
  description   = "BigQuery Table ID of the target table."
}