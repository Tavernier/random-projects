###############################################################################
# Terraform and Terragrunt Setup
###############################################################################
provider "google" {
  project = var.project
  region  = var.region
}

terraform {
  backend "gcs" {}
}

# we sometimes need the google project number and this block lets us do that.
# use ${data.google_project.project.number} where needed
data "google_project" "project" {
}

###############################################################################
# GCP Services
###############################################################################

# Warning: Do not disable or delete a service after enabling
# it. Bad things will happen such as built-in GCP service
# accounts getting deleted.
# Run gcloud services list --enabled to see list of enabled
# services and APIs. It might be more than the list below due
# to stuff GCP enables or folks enabled through the UI. For
# example, IAM gets enabled before I get access to the project.
resource "google_project_service" "this" {
  for_each = toset([
    "artifactregistry.googleapis.com",
    "bigquery.googleapis.com",
    "bigquerydatapolicy.googleapis.com",
    "cloudbuild.googleapis.com",
    "cloudfunctions.googleapis.com",
    "cloudscheduler.googleapis.com",
    "composer.googleapis.com",
    "datacatalog.googleapis.com",
    "dataform.googleapis.com",
    "datalineage.googleapis.com",
    "dataplex.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "pubsub.googleapis.com",
    "run.googleapis.com",
  ])
  project = var.project
  service = each.key
  disable_on_destroy = false
}

###############################################################################
# Other Stuff
###############################################################################

# We need this stuff before we can house or run code / applications in GCP.

resource "google_artifact_registry_repository" "docker" {
  project       = var.project
  location      = var.region
  repository_id = "docker-images"
  description   = "Docker Images."
  format        = "DOCKER"
  depends_on    = [ google_project_service.this ]
  labels        = {}

  docker_config {
    immutable_tags = true
  }
}

resource "google_artifact_registry_repository" "python" {
  project       = var.project
  location      = var.region
  repository_id = "python-libraries"
  description   = "Python Libraries."
  format        = "PYTHON"
  depends_on    = [ google_project_service.this ]
  labels        = {}
}


# GCP's PubSub service account agent needs permissions to manage BigQuery 
# in order for push-to-bq subscriptions to be created. If creating the actual
# PubSub subscriptions defined elsewhere in this file fails, try again later
# in order to give the security permissions time propogate.
data "google_project" "project" {
}

resource "google_project_iam_member" "viewer" {
  project = var.project
  role   = "roles/bigquery.metadataViewer"
  member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com"
}

resource "google_project_iam_member" "editor" {
  project = var.project
  role   = "roles/bigquery.dataEditor"
  member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com"
}
