variable "project" {
  type          = string
  description   = "GCP project id"
}

variable "region" {
  type          = string
  description   = "Default region in which stuff will be created."
  default       = "us-central1"
}
