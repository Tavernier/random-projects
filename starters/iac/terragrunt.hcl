remote_state {
  backend = "gcs"
  config = {
    bucket     = "gcs-stats-bucket"
    prefix     = "tfstate/project-name/${path_relative_to_include()}"
  }
}

inputs = {
  project  = get_env("GCP_PROJECT")
}
