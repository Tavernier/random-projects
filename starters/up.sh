#! /usr/bin/env bash

set -euo pipefail

start_app(){
    docker compose --profile=${DATA_STORE:-memory} up --build --detach
}

main(){
    start_app
}

main
